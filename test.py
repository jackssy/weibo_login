#! /usr/bin/env python
# -*- coding: utf-8 -*-
# __author__ = "ChenRenJian"
# Date: 2019/1/20

import requests
from bs4 import BeautifulSoup


############ 验证cookie是否爬取正确

def visit(cookie):
    home_page_url = 'https://weibo.cn/tybbs?f=search_0'
    headers = {"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
               "Accept-Encoding": "gzip, deflate, br",
               "Accept-Language": "zh-CN,zh;q=0.9",
               'Cache-Control': 'max-age=0',
               'Connection': 'keep-alive',
               'Cookie': cookie,
               # '_T_WM=2ca17cf0efbba661be4d6da7e28c80c2; SCF=ArGKbSnGAVlu-IjvBF3RGDg09c2bzR22-9r58auikzON9McrljfsHQkdzxD8I2IUMILeGgJgZ6FpKquHQUKJ6uY.; SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9WWTAhPF8SmODaqGbna.ZwMg5JpX5K-hUgL.FoeReheXSoq7e0z2dJLoIfQLxK-L1h-LB-BLxKBLBonLB-2LxKML1hzLBo.LxKqLBoqL1hnLxKML1h2LB-BLxKqL1hnL1K2LxKqL1h.L1h5LxKqL1-eL1h.LxKML1heLB-BLxKqL1heL1h-t; MLOGIN=1; M_WEIBOCN_PARAMS=oid%3D4326257380836527%26luicode%3D20000061%26lfid%3D4326257380836527; SUB=_2A25xPdS7DeRhGeVG61EV9ijMyD6IHXVSwfzzrDV6PUJbkdANLWfMkW1NT9NlMR_w9cc5SBjZuYzUBYI-ZM2MxB10; SUHB=06Z1KcO8IR4Vun; SSOLoginState=1547281644',

               "Host": 'weibo.cn',
               'Referer': 'https://weibo.cn/search/?pos=search',
               'Upgrade-Insecure-Requests': '1',
               "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36",
               # 'Connection':'close','verify':'false'
               }
    response = requests.get(url=home_page_url, headers=headers, verify=False).content
    # requests.adapters.DEFAULT_RETRIES = 5
    # soup = BeautifulSoup(response, "html.parser")
    # context = soup.select(".c")
    # context_all = []
    print('--------------登陆测试结果',response.decode('utf8'))

def login_test(cookie_dic):
    # cookie_dic = {'SUB': '_2A25xRZ55DeRhGeBP7FQW9CbLwzuIHXVSMoixrDV_PUJbm9AKLVH4kW9NRQG9aXj2w_UdjACDkR357FHbbrlk5SFZ',
    #               'LT': '1547824681',
    #               'SUBP': '0033WrSXqPxfM725Ws9jqgMF55529P9D9W5gjxajhCPd-JujMXyQM7p05NHD95QceKMcS0BRS0nNWs4Dqcjui--ciKL2i-2Ri--fi-2NiK.7i--Xi-zRiKy2i--fiK.0i-82eKzNeh.N1Btt'}
    #if isinstance(cookie_dic,dict):
    cookie = ''
    for x, y in cookie_dic.items():
        cookie += ''.join([x, '=', y, ';'])
    # else:
    #     cookie = cookie_dic
    visit(cookie)

import re
def get_follow():

    '''获取微博关注用户的uid与用户名'''
    url = 'http://weibo.com/' + '3803466032' + '/follow'
    cookies = 'SINAGLOBAL=9237842695891.799.1542530119296; un=rubaxxkcn@sina.com; ULV=1547368942829:5:3:1:6206403637698.29.1547368942813:1547264952064; UOR=,,login.sina.com.cn; SCF=AjE5_aMxEUJj1pIQHMPmRDMlH6KhLZBEca3KYf86zC47DAPnVeebe-3HHIBKxFDrMOrMg0ll17LCJrbun-fNJpU.; SUB=_2A25xQBcUDeRhGeVG61EV9ijMyD6IHXVSNA_crDV8PUJbmtAKLULQkW9NT9NlMXjtF8G2cjj-gG4SqoGGRGulwvQD; SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9WWTAhPF8SmODaqGbna.ZwMg5JpX5K2hUgL.FoeReheXSoq7e0z2dJLoIfQLxK-L1h-LB-BLxKBLBonLB-2LxKML1hzLBo.LxKqLBoqL1hnLxKML1h2LB-BLxKqL1hnL1K2LxKqL1h.L1h5LxKqL1-eL1h.LxKML1heLB-BLxKqL1heL1h-t; SUHB=0OMUFv7mSFSiH_; SSOLoginState=1547986756; wvr=6'
    html = requests.get(url, cookies=cookies).content

    c = html.find('member_ul clearfix') - 13
    html = html[c:]
    u = re.findall(r'[uid=]{4}([0-9]+)[&nick=]{6}(.*?)\\"', html)

    user_id = []
    uname = []
    for i in u:
        user_id.append(i[0])  # 把uid储存到列表user_id中
        uname.append(i[1])  # 把用户名储存到列表uname中

    print(user_id)
    print(uname)
    return user_id, uname  # 返回两个列表


if __name__ == '__main__':

    #
    #
    # cookie_dic ={'SUB': '_2A25xRZ55DeRhGeBP7FQW9CbLwzuIHXVSMoixrDV_PUJbm9AKLVH4kW9NRQG9aXj2w_UdjACDkR357FHbbrlk5SFZ', 'LT': '1547824681', 'SUBP': '0033WrSXqPxfM725Ws9jqgMF55529P9D9W5gjxajhCPd-JujMXyQM7p05NHD95QceKMcS0BRS0nNWs4Dqcjui--ciKL2i-2Ri--fi-2NiK.7i--Xi-zRiKy2i--fiK.0i-82eKzNeh.N1Btt'}
    #
    # cookie = ''
    # for x, y in cookie_dic.items():
    #     cookie += ''.join([x, '=', y, ';'])
    #
    # visit(cookie)

    get_follow()