#! /usr/bin/env python
# -*- coding: utf-8 -*-
# __author__ = "wangjing"
# Date: 2019/1/18

'''
参考：
https://blog.csdn.net/TYOUKAI_/article/details/78676480

1对应了新浪的预登录，2表示新浪的正式登录，3，表示获取weibo.com的跨域认证。4表示到weibo.cn的跨域认证。而我们所需要的就是3,4返回的cookie


代码参考：
https://blog.csdn.net/qq_42156420/article/details/82800536
'''

"""
python_weibo.py by xianhu
"""

import re
import rsa
import time
import json
import base64
import logging
import binascii
import requests
import urllib.parse

import image
from test import login_test
class WeiBoLogin(object):
    """
    class of WeiBoLogin, to login weibo.com
    """

    def __init__(self):
        """
        constructor
        """
        self.user_name = None
        self.pass_word = None
        self.user_uniqueid = None
        self.user_nick = None

        self.session = requests.Session()
        self.session.headers.update({"User-Agent": "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0"})
        self.session.get("http://weibo.com/login.php")
        return

    def login(self, user_name, pass_word):
        """
        login weibo.com, return True or False
        """
        self.user_name = user_name
        self.pass_word = pass_word
        self.user_uniqueid = None
        self.user_nick = None

        # get json data
        s_user_name = self.get_username()
        json_data = self.get_json_data(su_value=s_user_name)
        if not json_data:
            return False
        s_pass_word = self.get_password(json_data["servertime"], json_data["nonce"], json_data["pubkey"])

        # make post_data
        post_data = {
            "entry": "weibo",
            "gateway": "1",
            "from": "",
            "savestate": "7",
            "userticket": "1",
            "vsnf": "1",
            "service": "miniblog",
            "encoding": "UTF-8",
            "pwencode": "rsa2",
            "sr": "1280*800",
            "prelt": "529",
            "url": "http://weibo.com/ajaxlogin.php?framelogin=1&callback=parent.sinaSSOController.feedBackUrlCallBack",
            "rsakv": json_data["rsakv"],
            "servertime": json_data["servertime"],
            "nonce": json_data["nonce"],
            "su": s_user_name,
            "sp": s_pass_word,
            "returntype": "TEXT",
        }

        # 1 get captcha code
        if json_data["showpin"] == 1:
            url = "http://login.sina.com.cn/cgi/pin.php?r=%d&s=0&p=%s" % (int(time.time()), json_data["pcid"])
            #https://login.sina.com.cn/cgi/pin.php?p=yf-70b3300877a80dd03dfb38655d134ed9cc58

            with open("captcha.jpeg", "wb") as file_out:
                file_out.write(self.session.get(url).content)

            imgdata = open('captcha.jpeg', 'rb').read()
            code =  image.image_main(imgdata)
            post_data["pcid"] = json_data["pcid"]
            post_data["door"] = code

        #2  login weibo.com
        login_url_1 = "http://login.sina.com.cn/sso/login.php?client=ssologin.js(v1.4.18)&_=%d" % int(time.time())
        response1 = self.session.post(login_url_1, data=post_data)
        print('===============post_data',post_data)
        json_data_1 = response1.json()
        print('---------------json_data_1', response1.content.decode('gbk'))
        cookie = ''

        if json_data_1["retcode"] == "0":
            params = {
                "callback": "sinaSSOController.callbackLoginStatus",
                "client": "ssologin.js(v1.4.18)",
                "ticket": json_data_1["ticket"],
                "ssosavestate": int(time.time()),
                "_": int(time.time()*1000),
            }
            #3
            response = self.session.get("https://passport.weibo.com/wbsso/login", params=params)
            json_data_2 = json.loads(re.search(r"\((?P<result>.*)\)", response.text).group("result"))
            print('---------------json_data_2', response.text.encode('utf8'))
            params_cn = {
                'action': 'login' ,
               'savestate' : 1 ,
                'callback' : 'sinaSSOController.doCrossDomainCallBack' ,
               'client' : "ssologin.js(v1.4.18)",
                "_": int(time.time() * 1000),
            }

            #-4-----------------------------
            # response_cn = self.session.get("https://passport.weibo.cn/sso/crossdomain", params=params_cn)
            # json_data_cn = json.loads(re.search(r"\((?P<result>.*)\)", response_cn.text).group("result"))
            # print('---------------json_data_2', response_cn.text)


            if json_data_2["result"] is True:
                cookie_dic = self.session.cookies.get_dict()

                cookie = ''
                for x, y in cookie_dic.items():
                    cookie += ''.join([x, '=', y, ';'])

                url_1 = 'https://login.sina.com.cn/sso/login.php?url=https://m.weibo.cn/status/4326320126776950?&_rand=1547987043.5922&gateway=1&service=sinawap&entry=sinawap&useticket=1&returntype=META&sudaref=&_client_version=0.6.29'
                headers = {'cookie': cookie}
                cookie = self.session.get(url_1, headers=headers).cookies.get_dict()


        return cookie#True if self.user_uniqueid and self.user_nick else False

    def get_username(self):
        """
        get legal username
        """
        username_quote = urllib.parse.quote_plus(self.user_name)
        username_base64 = base64.b64encode(username_quote.encode("utf-8"))
        return username_base64.decode("utf-8")

    def get_json_data(self, su_value):
        """
        get the value of "servertime", "nonce", "pubkey", "rsakv" and "showpin", etc
        """
        params = {
            "entry": "weibo",
            "callback": "sinaSSOController.preloginCallBack",
            "rsakt": "mod",
            "checkpin": "1",
            "client": "ssologin.js(v1.4.18)",
            "su": su_value,
            "_": int(time.time()*1000),
        }
        try:
            response = self.session.get("http://login.sina.com.cn/sso/prelogin.php", params=params)
            json_data = json.loads(re.search(r"\((?P<data>.*)\)", response.text).group("data"))
        except Exception as excep:
            json_data = {}
            logging.error("WeiBoLogin get_json_data error: %s", excep)

        logging.debug("WeiBoLogin get_json_data: %s", json_data)
        return json_data

    def get_password(self, servertime, nonce, pubkey):
        """
        get legal password
        """
        string = (str(servertime) + "\t" + str(nonce) + "\n" + str(self.pass_word)).encode("utf-8")
        public_key = rsa.PublicKey(int(pubkey, 16), int("10001", 16))
        password = rsa.encrypt(string, public_key)
        password = binascii.b2a_hex(password)
        return password.decode()


def main(user,password):
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s\t%(levelname)s\t%(message)s")
    weibo = WeiBoLogin()
    weibo_login = weibo.login(user, password)

    print('=======================cookie',weibo_login)

    return weibo_login

if __name__ == "__main__":
    #
    # logging.basicConfig(level=logging.DEBUG, format="%(asctime)s\t%(levelname)s\t%(message)s")
    # weibo = WeiBoLogin()
    # weibo_login = weibo.login("rubaxxkcn@sina.com", "jks232")

    #weibo_login = main("rubaxxkcn@sina.com", "jks232")
    weibo_login = main('18649714783', 'jing123456')
    #login_test(weibo_login)
