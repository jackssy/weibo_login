#! /usr/bin/env python
# -*- coding: utf-8 -*-
# __author__ = "ChenRenJian"
# Date: 2019/1/18

import os

from flask import request
from flask_restful import Resource, Api
import flask_cors
from werkzeug.contrib.fixers import ProxyFix
from flask import Flask

'''
微博登录----含验证码----输出cookie

接口：127.0.0.1：5000/?user=jdfdsf&password=dfsdfa
'''
from login import main

app = Flask(__name__)  ##实例化app对象

flask_cors.CORS(app, supports_credentials=True)  # 局域网访问
app.config.update(RESTFUL_JSON=dict(ensure_ascii=False))  ##传参存在中文的处理
api = Api(app)


class Lyric(Resource):

    def get(self):

        response = {}

        user = request.args.get('user')
        password = request.args.get('password')


        try:
            lrc = main(user, password)
            if lrc:
                response['success'] = True
                response['cookie'] = lrc
            else:
                response['success'] = False
                response['errorText'] = '二维码识别错误'

            return response


        except Exception as e:
            response['success'] = False
            response['err'] = str(e)
            response['errorText'] = '获取cookie识别'
            return response


api.add_resource(Lyric, '/')


if __name__ == '__main__':
    #test()
     #http://127.0.0.1:5000/?user=18649714783&password=jing12356
    app.wsgi_app = ProxyFix(app.wsgi_app)
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    app.run(host='127.0.0.1', port=5000, debug=True)


