#! /usr/bin/env python
# -*- coding: utf-8 -*-
# __author__ = "Q1mi"
# Date: 2019/1/11
#微博自动登陆 -----------只限于无验证码的微博登陆---wap端

import requests
from bs4 import BeautifulSoup
import json
from test import login_test
def login(username,password):

    url = r'https://passport.weibo.cn/sso/login'
    # 构造参数字典
    data = {'username': username,
            'password': password,
            'savestate': '1',
            'r': r'',
            'ec': '0',
            'pagerefer': '',
            'entry': 'mweibo',
            'wentry': '',
            'loginfrom': '',
            'client_id': '',
            'code': '',
            'qq': '',
            'mainpageflag': '1',
            'hff': '',
            'hfp': ''}
    # headers，防屏
    headers = {
        'User-Agent': 'Mozilla/5.0 (Linux; U; Android 2.3.6; en-us; Nexus S Build/GRK39F) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',#'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36',
        'Accept': 'text/html;q=0.9,*/*;q=0.8',
        'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
        'Connection': 'close',
        'Referer': 'https://passport.weibo.cn/signin/login?entry=mweibo&r=https%3A%2F%2Fweibo.cn%2F&backTitle=%CE%A2%B2%A9&vt=',#'https://passport.weibo.cn/signin/login',
        'Host': 'passport.weibo.cn'
        }
    # 模拟登录geetest_wait

    session = requests.Session()
    r = session.post(url, data=data, headers=headers)
    jsonStr = r.content.decode('gbk')
    info = json.loads(jsonStr)
    print ('info=',info)
    if info["retcode"] == 20000000:
        cookie = session.cookies.get_dict()
        flag = True
        print(cookie)
    else:
        cookie = "Failed!( Reason:%s )" % info['msg']
        flag = False
        print(cookie)
    # cookies1 = ''
    # for x,y in cookies[0].items():
    #     cookies1 += ''.join([x,'=',y,';'])
    #
    # print(cookies[0])
    # print(cookies1.encode('utf8'))
    return cookie,flag


if __name__ == '__main__':
   # cookie, flag = login('scxaaixipi@sina.com','934883296')
    cookie, flag = login('18649714783', 'jing123456')
    login_test(cookie)